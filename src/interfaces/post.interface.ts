export interface PostInterface {
    id?: string;
    is_active: boolean,
    description: string,
    name: string,
    type: []
}