import Posts from "../views/Posts";
import AddPost from "../views/AddPost";
import EditPost from "../views/EditPost";

export default [
    {
        path: "/",
        component: Posts,
        in_nav: true,
        name: "Posts"
    },
    {
        path: "/add",
        in_nav: true,
        component: AddPost,
        name: "Add Post"
    },
    {
        path: "/edit/:id",
        in_nav: false,
        component: EditPost,
        name: "Edit Post"
    }
];
