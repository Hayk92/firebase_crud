import React from "react";
import {useSelector} from "react-redux";
import {useFirestore, useFirestoreConnect} from "react-redux-firebase";
import {PostInterface} from "../interfaces/post.interface";
import EditPostForm from "../components/forms/EditPostForm";

const EditPost = ({match, history}: any) => {
    const submit = (data: PostInterface) => {
        editPost(data);
    };

    const firestore = useFirestore();

    useFirestoreConnect({
        collection: "posts",
        doc: match.params.id
    });

    const post = useSelector((state: any) => state.firestore.data.posts);

    const editPost = (post: PostInterface) => {
        firestore
            .collection("posts")
            .doc(match.params.id)
            .update({
                ...post
            })
            .then(docRef => {
                history.push("/");
            }).catch(err => console.log(err));
    };

    return (
        <>
            <EditPostForm id={match.params.id} post={post} onSubmit={submit}/>
        </>
    );
};

export default EditPost;
