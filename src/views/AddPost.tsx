import React from "react";
import {useFirestore} from "react-redux-firebase";
import {PostInterface} from "../interfaces/post.interface";
import AddPostForm from "../components/forms/AddPostForm";

const AddPost = ({history}: any) => {
    const submit = (data: PostInterface) => {
        addPost(data);
    };

    const firestore = useFirestore();

    const addPost = (post: PostInterface) => {
        firestore
            .collection("posts")
            .add({
                ...post
            })
            .then(() => {
                history.push("/")
            }).catch(err => console.log(err));
    };

    return (
        <>
            <AddPostForm onSubmit={submit}/>
        </>
    );
};

export default AddPost;
