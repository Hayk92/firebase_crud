import React from "react";
import {ListItem, ListItemText, IconButton, ListItemIcon, Typography} from "@material-ui/core";
import {Delete, Edit} from "@material-ui/icons";
import {PostInterface} from "../interfaces/post.interface";


const PostList = (props: any) => {
    const {posts, onDelete, history} = props;
    return (
        <>
            {posts && posts.map((p: PostInterface, index: number) => (
                <ListItem key={index}>
                    <ListItemText
                        primary={p.name}
                        secondary={
                            <>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    color="textPrimary"
                                >
                                    {p.is_active? "Active": "Not Active"}
                                </Typography>
                                <br/>
                                {p.description}
                            </>
                        }
                    />

                    <ListItemIcon>
                        <IconButton onClick={() => history.push(`/edit/${p.id}`)} aria-label="Edit">
                            <Edit color="primary" />
                        </IconButton>
                    </ListItemIcon>
                    <ListItemIcon>
                        <IconButton onClick={() => onDelete(p.id)} edge="end" aria-label="Delete">
                            <Delete color="secondary" />
                        </IconButton>
                    </ListItemIcon>
                </ListItem>
            ))}
        </>
    );
};

export default PostList;
