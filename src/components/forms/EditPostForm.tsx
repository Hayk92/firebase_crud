import React from "react";
import {Form, Field} from "react-final-form";
import {Input, TextField, Checkbox} from "final-form-material-ui";
import {Button, Chip, MenuItem} from "@material-ui/core";
import MultiSelect from "../MultiSelect";


const EditPostForm = (props: any) => {
    const options = {
        1: "Food",
        2: "Style",
        3: "Just"
    };

    const {id, post, onSubmit} = props;

    return (
        <>
            <Form
                onSubmit={onSubmit}
                render={({handleSubmit, invalid, pristine}) => {
                    return (
                        post && <form onSubmit={handleSubmit}>
                            <div>
                                <Field name="name"
                                       component={Input}
                                       className="input"
                                       type="text"
                                       placeholder="Name"
                                       defaultValue={post[id].name}
                                />
                            </div>
                            <div>
                                <Field name="description"
                                       component={TextField}
                                       className="text"
                                       type="text"
                                       placeholder="Description"
                                       defaultValue={post[id].description}
                                />
                            </div>

                            <div>
                                <Field name="type"
                                       style={{width: 200}}
                                       styling="field"
                                       labelname="Types"
                                       initialValue={post[id].type}
                                       component={MultiSelect}
                                       renderValue={(selected: []) => (
                                           <div className="multi-select-chips">
                                               {[...selected].map((value) => {
                                                   return (

                                                   <Chip
                                                       key={value}
                                                       label={options[value]}
                                                       className="multi-select-chip"
                                                   />
                                               )})}
                                           </div>
                                       )}
                                >
                                    {Object.entries(options).map(([key, value], index) => (
                                        <MenuItem
                                            key={index} value={parseInt(key)}>
                                            {value}
                                        </MenuItem>
                                    ))}
                                </Field>
                            </div>

                            <div>
                                <label>
                                    <Field name="is_active"
                                           component={Checkbox}
                                           className="checkbox"
                                           type="checkbox"
                                           defaultValue={post[id].is_active}
                                    />
                                    Active
                                </label>
                            </div>
                            <Button type="submit" variant="contained" color="primary" disabled={pristine || invalid}>
                                Submit
                            </Button>
                        </form>
                    )
                }}
            >
            </Form>
        </>
    )
};

export default EditPostForm;
