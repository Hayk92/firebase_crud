import React from "react";
import {HashRouter as Router, Route, Link, Switch} from "react-router-dom";
import route from "./route";

const Navigation = () => {
    return (
        <>
            <Router>
                <div>
                    <nav>
                        <ul className="navigation">
                            {route.map((r, index) =>
                                r.in_nav && <li key={index}><Link to={r.path}>{r.name}</Link></li>
                            )}
                        </ul>
                    </nav>
                    <Switch>
                        {route.map((r, index) =>
                            <Route exact path={r.path} component={r.component} key={index}/>
                        )}
                    </Switch>
                </div>
            </Router>
        </>
    );
};

export default Navigation;
