import React from "react";
import {useSelector} from "react-redux";
import {useFirestoreConnect, useFirestore} from "react-redux-firebase";
import {Container, List} from "@material-ui/core";
import PostList from "../components/PostList";
import {withRouter} from "react-router"


const Posts = ({history}: any) => {
    useFirestoreConnect({
        collection: "posts",
        storeAs: "posts"
    });

    const firestore = useFirestore();
    let data = useSelector((state: any) => state.firestore.ordered.posts);
    const dropPost = (id: string) => {
        firestore
            .collection("posts")
            .doc(id)
            .delete();
    };

    return (
        <Container>
            <List>
                <PostList history={history} onDelete={dropPost} posts={data}/>
            </List>
        </Container>
    );
};

export default withRouter(Posts);
